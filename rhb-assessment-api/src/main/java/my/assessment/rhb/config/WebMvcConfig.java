package my.assessment.rhb.config;


import java.util.Locale;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;

import my.assessment.rhb.util.RHBUtil;


@Configuration
public class WebMvcConfig extends WebMvcConfigurerAdapter {

	@Bean
	public RHBUtil rhbUtil() {
		return new RHBUtil();
	}


	@Bean
	public MessageSource messageSource() {
		ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
		messageSource.setBasename("classpath:messages/messages");
		return messageSource;
	}


	@Bean
	public LocaleResolver localeResolver() {
		CookieLocaleResolver resolver = new CookieLocaleResolver();
		resolver.setDefaultLocale(new Locale("ms"));
		resolver.setCookieName("rhbAdminApi");
		resolver.setCookieMaxAge(4800);
		return resolver;
	}

}
