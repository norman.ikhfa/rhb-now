package my.assessment.rhb.config;


import java.util.ArrayList;
import java.util.List;

import org.audit4j.core.handler.Handler;
import org.audit4j.core.layout.CustomizableLayout;
import org.audit4j.integration.spring.AuditAspect;
import org.audit4j.integration.spring.SpringAudit4jConfig;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import my.assessment.rhb.audit.AuditHandler;
import my.assessment.rhb.component.AuditMetaData;


@Configuration
@EnableAspectJAutoProxy(proxyTargetClass = true)
@EnableConfigurationProperties
public class AuditConfig {

	private static final String DEFAULT_TEMPLATE = "${eventDate}|${uuid}|${actor}|${action}|${origin} => ${foreach fields field}${field.name} :${field.value}, ${end}";


	@Bean
	public AuditAspect auditAspect() {
		AuditAspect auditAspect = new AuditAspect();
		return auditAspect;
	}


	@Bean
	public AuditHandler auditHandler() {
		return new AuditHandler();
	}


	@Bean
	public SpringAudit4jConfig springAudit4jConfig() {
		SpringAudit4jConfig springAudit4jConfig = new SpringAudit4jConfig();

		CustomizableLayout customlayout = new CustomizableLayout();
		customlayout.setTemplate(DEFAULT_TEMPLATE);
		springAudit4jConfig.setLayout(customlayout);

		List<Handler> handlers = new ArrayList<Handler>();
		handlers.add(auditHandler());
		springAudit4jConfig.setHandlers(handlers);
		springAudit4jConfig.setMetaData(new AuditMetaData());
		return springAudit4jConfig;
	}

}
