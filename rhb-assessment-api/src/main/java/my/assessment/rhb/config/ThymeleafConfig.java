package my.assessment.rhb.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.thymeleaf.spring4.templateresolver.SpringResourceTemplateResolver;
import org.thymeleaf.templateresolver.TemplateResolver;


@Configuration
public class ThymeleafConfig {

	@Bean(name = "emailTemplate")
	public TemplateResolver emailTemplateResolver() {
		SpringResourceTemplateResolver resolver = new SpringResourceTemplateResolver();
		resolver.setPrefix("classpath:/emails/");
		resolver.setSuffix(".html");
		resolver.setOrder(0);
		return resolver;
	}

}
