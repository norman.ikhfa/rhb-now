package my.assessment.rhb.rest.module;


import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.audit4j.core.dto.EventBuilder;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import lombok.extern.slf4j.Slf4j;
import my.assessment.rhb.constant.AccessLevel;
import my.assessment.rhb.constant.AuditType;
import my.assessment.rhb.db.model.Person;
import my.assessment.rhb.db.service.PersonService;
import my.assessment.rhb.util.RHBModule;
import my.assessment.rhb.util.JsonOutput;


@RestController
@RequestMapping("/persons")
@RHBModule(accessLevel = { AccessLevel.VIEW, AccessLevel.ADD, AccessLevel.UPDATE,
		AccessLevel.DELETE }, moduleName = "Module - Person", coreType = false, rootUrl = "/persons")
@Slf4j
public class PersonController extends BaseController {

	@Autowired
	private PersonService personSvc;


	@Override
	@RequestMapping(value = "save", method = RequestMethod.POST)
	public JsonOutput<?> saveRecord(@RequestBody String formData, HttpServletRequest request,
			HttpServletResponse response, Locale locale) throws IOException, JSONException {
		JsonOutput<?> output = new JsonOutput<>();
		String username = getAuthenticateUser(request);

		try {

			status = false;

			// to add record
			JSONObject data = new JSONObject(formData);

			Person record = new Person();
			record.setCreatedBy(username);
			record.setCreatedDate(new Date());
			record.setFirstName(data.getString("firstName"));
			record.setLastName(data.getString("lastName"));
			record.setEmail(data.getString("email"));
			record.setUpdatedBy(username);
			record.setUpdatedDate(new Date());

			record = personSvc.save(record);

			rhbUtil.addEvent(new EventBuilder().addAction("event.add.record.person").addField("data", record)
					.addActor(username).addTag(AuditType.PERSON.getValue()).build());

			status = true;
			message = "message.info.save.success";

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Internal Error - {}", e.getMessage());
			message = "message.internal.server.error";
		}

		output.setStatus(status);
		output.setMessage(
				message != null && message.length() > 0 ? messageSource.getMessage(message, null, locale) : "OK");

		return output;
	}


	@RequestMapping(value = "get_all_records", method = RequestMethod.GET)
	@JsonView(JsonOutput.View.class)
	public JsonOutput<?> getAllRecords(HttpServletRequest request, HttpServletResponse response, Locale locale)
			throws IOException, JSONException {
		JsonOutput<Person> output = new JsonOutput<>();
		status = false;
		message = "";

		try {

			List<Person> records = personSvc.findAll();
			if (records != null) {
				output.setData(records);
				output.setRecordsTotal(records.size());
				status = true;
			} else {
				message = "message.no.record.found";
				status = false;
			}

		} catch (Exception e) {
			log.error("Internal Error - {}", e.getMessage());
			message = "message.internal.server.error";
		}

		output.setStatus(status);
		output.setMessage(
				message != null && message.length() > 0 ? messageSource.getMessage(message, null, locale) : "OK");

		return output;
	}


	@RequestMapping(value = "get_record_by_first_name/{name}", method = RequestMethod.GET)
	public JsonOutput<?> getRecordByFirstName(@PathVariable("name") String name, HttpServletRequest request,
			HttpServletResponse response, Locale locale) throws IOException, JSONException {
		JsonOutput<Person> output = new JsonOutput<>();
		status = false;
		message = "";

		try {
			List<Person> records = null;

			if (name != null && !name.equals("")) {
				records = personSvc.findByFirstName(name);
				if (records != null && records.size() > 0) {
					output.setData(records);
					status = true;
				} else {
					message = "message.no.record.found";
					status = false;
				}

			} else {
				message = "message.no.record.found";
				status = false;
			}

		} catch (Exception e) {
			log.error("Internal Error - {}", e.getMessage());
			message = "message.internal.server.error";
		}

		output.setStatus(status);
		output.setMessage(
				message != null && message.length() > 0 ? messageSource.getMessage(message, null, locale) : "OK");

		return output;
	}


	@RequestMapping(value = "update/{name}", method = RequestMethod.PUT)
	@JsonView(JsonOutput.View.class)
	public JsonOutput<?> updateUser(@PathVariable("name") String name, @RequestBody String formData,
			HttpServletRequest request, HttpServletResponse response, Locale locale) {
		String username = getAuthenticateUser(request);
		JsonOutput<?> output = new JsonOutput<>();
		status = false;

		try {
			JSONObject data = new JSONObject(formData);

			if (name != null && !name.equals("")) {
				List<Person> records = personSvc.findByFirstName(name);
				if (records != null && records.size() > 0) {
					for (Person record : records) {
						if (data.has("firstName")) {
							record.setFirstName(data.getString("firstName"));
						}
						if (data.has("lastName")) {
							record.setLastName(data.getString("lastName"));
						}
						if (data.has("email")) {
							record.setEmail(data.getString("email"));
						}
						record.setUpdatedBy(username);
						record.setUpdatedDate(new Date());

						personSvc.save(record);
					}
					status = true;
					message = "message.info.save.success";
				} else {
					message = "message.no.record.found";
					status = false;
				}
			} else {
				status = false;
				message = "message.bad.request";
			}
		} catch (Exception e) {
			log.error("Internal Error - {}", e.getMessage());
			message = "message.internal.server.error";
		}

		output.setStatus(status);
		output.setMessage(
				message != null && message.length() > 0 ? messageSource.getMessage(message, null, locale) : "OK");

		return output;
	}


	@Override
	@RequestMapping(value = "delete_record/{name}", method = RequestMethod.DELETE)
	public JsonOutput<?> deleteRecord(@PathVariable("name") String name, HttpServletRequest request,
			HttpServletResponse response, Locale locale) throws IOException, JSONException {
		JsonOutput<?> output = new JsonOutput<>();

		try {
			status = false;
			message = "";

			if (name != null && !name.equals("")) {
				List<Person> records = personSvc.findByFirstName(name);
				if (records != null && records.size() > 0) {
					for (Person record : records) {
						status = personSvc.delete(record.getId());
					}
				} else {
					status = false;
				}
				message = status ? "message.success.delete" : "message.fail.delete";
			} else {
				status = false;
				message = "message.bad.request";
			}

		} catch (Exception e) {
			if (e instanceof DataIntegrityViolationException) {
				message = "message.fail.delete.constraint";

			} else {
				message = "message.internal.server.error";
			}

		}

		output.setStatus(status);
		output.setMessage(
				message != null && message.length() > 0 ? messageSource.getMessage(message, null, locale) : "OK");

		return output;
	}


	@Override
	public JsonOutput<?> getTableRecords(String state, HttpServletRequest request, HttpServletResponse response,
			Locale locale) throws IOException, JSONException {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public JsonOutput<?> getRecord(String data, HttpServletRequest request, HttpServletResponse response,
			Locale locale) throws IOException, JSONException {
		// TODO Auto-generated method stub
		return null;
	}

}
