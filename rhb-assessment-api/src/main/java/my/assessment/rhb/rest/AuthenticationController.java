package my.assessment.rhb.rest;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.audit4j.core.dto.EventBuilder;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.mobile.device.Device;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;
import my.assessment.rhb.constant.AuditType;
import my.assessment.rhb.db.model.Module;
import my.assessment.rhb.db.model.ModulePermission;
import my.assessment.rhb.db.model.RHBUser;
import my.assessment.rhb.db.model.UserAccount;
import my.assessment.rhb.db.model.UserRole;
import my.assessment.rhb.db.service.IUserDetailsService;
import my.assessment.rhb.db.service.ModuleService;
import my.assessment.rhb.db.service.PermissionService;
import my.assessment.rhb.db.service.UserAccountService;
import my.assessment.rhb.rest.module.BaseController;
import my.assessment.rhb.security.RHBAuthenticationResponse;
import my.assessment.rhb.security.RHBTokenUtil;
import my.assessment.rhb.util.JsonOutput;
import my.assessment.rhb.util.RHBUtil;


@RestController
@RequestMapping("/auth")
@Slf4j
public class AuthenticationController extends BaseController {

	@Value("${rhb.jwt.header}")
	private String tokenHeader;

	protected String message = "";

	protected boolean status = false;

	@Autowired
	private RHBTokenUtil rhbTokenUtil;

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private IUserDetailsService userDetailsService;

	@Autowired
	protected MessageSource messageSource;

	@Autowired
	protected RHBTokenUtil tokenUtil;

	@Autowired
	protected RHBUtil rhbUtil;

	@Autowired
	private UserAccountService userAccountService;

	@Autowired
	private PermissionService permissionService;

	@Autowired
	private ModuleService moduleService;

	@RequestMapping(value = "get_login", method = RequestMethod.POST)
	public ResponseEntity<?> loadUser(@RequestBody String input, HttpServletResponse response, Locale locale,
			Device device) throws AuthenticationException, IOException, JSONException {

		JSONObject data = getData(input, response, locale);

		if (!data.has("loginName")) {
			response.sendError(HttpServletResponse.SC_BAD_REQUEST,
					messageSource.getMessage("message.bad.request", null, locale));
		}

		// Reload password post-security so we can generate token
		final RHBUser userDetails;
		if (data.getString("loginName").equals("sysoperator")) {
			userDetails = (RHBUser) userDetailsService.loadUserByUsername(data.getString("loginName"));
		} else {
			userDetails = (RHBUser) userDetailsService.loadUserByUsername(data.getString("loginName"));
		}

		JsonOutput<UserAccount> output = new JsonOutput<UserAccount>();
		output.setRecord(userDetails.getUserAccount());
		output.setStatus(userDetails != null ? true : false);

		// Return the token
		return ResponseEntity.ok(output);
	}


	@RequestMapping(value = "login", method = RequestMethod.POST)
	public ResponseEntity<?> createAuthenticationToken(@RequestBody String input, HttpServletResponse response,
			Locale locale, Device device) throws AuthenticationException, IOException, JSONException {

		JSONObject data = getData(input, response, locale);

		if (!data.has("loginName")) {
			response.sendError(HttpServletResponse.SC_BAD_REQUEST,
					messageSource.getMessage("message.bad.request", null, locale));
		}

		final Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(data.getString("loginName"), data.getString("password")));
		SecurityContextHolder.getContext().setAuthentication(authentication);

		// Reload password post-security so we can generate token
		final RHBUser userDetails = (RHBUser) userDetailsService.loadUserByUsername(data.getString("loginName"));

		// allow sysoperator only

		if (!data.has("sso")
				&& !userDetails.getUserAccount().getUserRole().getCode().equalsIgnoreCase("sysoperator")) {

			if (userDetails.getUserAccount() == null) {
				rhbUtil.addEvent(new EventBuilder().addAction("event.login.fail")
						.addField("loginName", userDetails.getUsername()).addActor(userDetails.getUsername())
						.addTag(AuditType.SECURITY.getValue()).build());

				throw new UsernameNotFoundException(String.format(
						"User with login name '%s' are not allowed to login.", data.getString("loginName")));
			}
		}

		final String token = rhbTokenUtil.generateToken(userDetails, device);

		List<String> userRoles = userDetails.getAuthorities().stream()
				.map(authority -> authority.getAuthority().toUpperCase()).collect(Collectors.toList());

		rhbUtil.addEvent(
				new EventBuilder().addAction("event.login.success").addField("loginName", userDetails.getUsername())
						.addActor(userDetails.getUsername()).addTag(AuditType.SECURITY.getValue()).build());

		// Return the token
		return ResponseEntity.ok(new RHBAuthenticationResponse(token, userDetails.getUserAccount().getId(),
				userDetails.getUserAccount().getFullName(), userRoles));
	}


	@RequestMapping(value = "has_permission/{param}", method = RequestMethod.GET)
	public JsonOutput<?> hasPermission(@PathVariable("param") String param, HttpServletRequest request,
			HttpServletResponse response, Locale locale) throws IOException, JSONException {
		JsonOutput<ModulePermission> output = new JsonOutput<>();
		boolean status = false;
		String message = "";

		String token = request.getHeader(tokenHeader);
		if (token != null && token.startsWith("Bearer ")) {
			token = token.substring(7);
		} else {
			output.setStatus(status);
			output.setMessage("Tiada kebenaran untuk capaian");
			return output;
		}

		try {
			String username = tokenUtil.getUsernameFromToken(token);
			JSONObject data = getData(param, response, locale);

			if (username.equals(data.getString("userId"))) {
				String controllerName = data.getString("module");
				String permission = data.getString("accessLevel");

				if (username.equals("sysoperator")) {
					log.debug(String.format("Bypass permission for sysoperator: %s: %s", controllerName,
							permission));
					output.setStatus(true);
					output.setMessage("OK");
					return output;
				}

				UserAccount user = userAccountService.findByLoginName(username);
				List<UserRole> userRoles = new ArrayList<UserRole>();
				userRoles.add(user.getUserRole());

				Module module = moduleService.findByControllerName(controllerName);
				status = permissionService.hasPermission(module, permission, userRoles);

			} else {
				status = false;
				message = "message.permission.denied";
			}

		} catch (Exception e) {
			e.printStackTrace();
			status = false;
			message = "message.internal.server.error";
			log.error("Error on checking has permission - {}", e.getMessage());

		}

		output.setStatus(status);
		output.setMessage(
				message != null && message.length() > 0 ? messageSource.getMessage(message, null, locale) : "OK");
		return output;
	}


	@RequestMapping(value = "get_permission/{param}", method = RequestMethod.GET)
	public JsonOutput<?> getPermission(@PathVariable("param") String param, HttpServletRequest request,
			HttpServletResponse response, Locale locale) throws IOException, JSONException {
		JsonOutput<ModulePermission> output = new JsonOutput<>();
		boolean status = false;
		String message = "";

		String token = request.getHeader(tokenHeader);
		if (token != null && token.startsWith("Bearer ")) {
			token = token.substring(7);
		} else {
			output.setStatus(status);
			output.setMessage("Tiada kebenaran untuk capaian");
			return output;
		}

		try {
			String username = tokenUtil.getUsernameFromToken(token);
			JSONObject data = getData(param, response, locale);

			if (username.equals(data.getString("loginName"))) {
				String module = data.getString("module");

				UserAccount user = userAccountService.findByLoginName(username);
				List<UserRole> userRoles = new ArrayList<UserRole>();
				userRoles.add(user.getUserRole());

				List<ModulePermission> mps = permissionService.getModulePermission(module, userRoles);

				output.setData(mps);
				status = true;

			} else {
				status = false;
				message = "message.permission.denied";
			}

		} catch (Exception e) {
			e.printStackTrace();
			status = false;
			message = "message.internal.server.error";
			log.error("Error on checking has permission - {}", e.getMessage());

		}

		output.setStatus(status);
		output.setMessage(
				message != null && message.length() > 0 ? messageSource.getMessage(message, null, locale) : "OK");
		return output;
	}


	@Override
	public JsonOutput<?> getTableRecords(String state, HttpServletRequest request, HttpServletResponse response,
			Locale locale) throws IOException, JSONException {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public JsonOutput<?> getRecord(String data, HttpServletRequest request, HttpServletResponse response,
			Locale locale) throws IOException, JSONException {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public JsonOutput<?> deleteRecord(String data, HttpServletRequest request, HttpServletResponse response,
			Locale locale) throws IOException, JSONException {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public JsonOutput<?> saveRecord(String formData, HttpServletRequest request, HttpServletResponse response,
			Locale locale) throws IOException, JSONException {
		// TODO Auto-generated method stub
		return null;
	}

}
