package my.assessment.rhb.rest;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.web.bind.annotation.RequestBody;

import my.assessment.rhb.security.RHBTokenUtil;
import my.assessment.rhb.util.RHBUtil;
import my.assessment.rhb.util.JsonOutput;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class BaseController {

	protected String message = "";
	protected boolean status = false;

	@Autowired
	protected MessageSource messageSource;

	@Autowired
	protected RHBUtil rhbUtil;

	@Value("${rhb.jwt.header}")
	private String tokenHeader;

	@Autowired
	protected RHBTokenUtil tokenUtil;

	public abstract JsonOutput<?> getTableRecords(String state, HttpServletResponse response, Locale locale)
			throws IOException, JSONException;

	public abstract JsonOutput<?> getRecord(String data, HttpServletResponse response, Locale locale)
			throws IOException, JSONException;

	public abstract ResponseEntity<String> saveRecord(@RequestBody String formData, HttpServletRequest request,
			HttpServletResponse response, Locale locale) throws IOException, JSONException;

	public abstract ResponseEntity<String> deleteRecord(@RequestBody String data, HttpServletRequest request,
			HttpServletResponse response, Locale locale) throws IOException, JSONException;

	protected JSONObject getData(String data, HttpServletResponse response, Locale locale) throws IOException {
		JSONObject obj = null;

		if (data == null || data.isEmpty()) {
			log.error("Bad request with no input data");
			response.sendError(HttpServletResponse.SC_BAD_REQUEST,
					messageSource.getMessage("message.bad.request", null, locale));
		} else {

			try {
				byte[] paramByte = Base64.decode(data.getBytes());
				String paramStr = new String(paramByte);
				obj = new JSONObject(paramStr);
			} catch (Exception e) {
				log.error("Error on translating data - {}", e.getMessage());
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
						messageSource.getMessage("message.internal.server.error", null, locale));
			}

		}

		return obj;
	}

	protected String getAuthenticateUser(HttpServletRequest request) {
		String token = request.getHeader(tokenHeader);
		if (token != null && token.startsWith("Bearer ")) {
			token = token.substring(7);
		}
		String username = tokenUtil.getUsernameFromToken(token);
		return username;
	}

	protected String getClientUserApplicationURL(String dataObj) {

		JSONObject objClient = null;
		String url = "";
		try {

			byte[] paramByte = Base64.decode(dataObj.getBytes());
			String paramStr = new String(paramByte);
			objClient = new JSONObject(paramStr);
			url = objClient.getString("url");

			return url;

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Internal Error - {}", e.getMessage());
		}

		return "";
	}

	protected String recheck(String data, HttpServletRequest request, HttpServletResponse response, Locale locale)
			throws IOException, JSONException {
		JSONObject objData = getData(data, response, locale);

		return encodeData(objData.toString());
	}

	protected String encodeData(String rawData) {
		return new String(Base64.encode(rawData.getBytes()));
	}

	protected HttpHeaders getAuthorizationHeader(HttpServletRequest request) {
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", getAuthorizationRequestHeader(request));

		return headers;

	}

	protected String getAuthorizationRequestHeader(HttpServletRequest request) {
		String token = request.getHeader(tokenHeader);

		return token;
	}

}
