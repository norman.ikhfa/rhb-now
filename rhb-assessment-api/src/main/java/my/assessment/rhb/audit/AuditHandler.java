package my.assessment.rhb.audit;


import org.audit4j.core.dto.AuditEvent;
import org.audit4j.core.dto.Field;
import org.audit4j.core.exception.HandlerException;
import org.audit4j.core.exception.InitializationException;
import org.audit4j.core.handler.Handler;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import lombok.extern.slf4j.Slf4j;


@EnableConfigurationProperties
@Slf4j
public class AuditHandler extends Handler {

	private static final long serialVersionUID = 1L;

	@Value("${rhb.audit.log.enabled}")
	private boolean logEnabled;


	@Override
	public void init() throws InitializationException {
	}


	@Override
	public void stop() {
	}


	@Override
	public void handle() throws HandlerException {
		if (logEnabled) {
			writeEvent(getAuditEvent());
		}
	}


	public boolean writeEvent(AuditEvent event) throws HandlerException {
		JSONObject obj = new JSONObject();

		for (Field element : event.getFields()) {
			try {
				obj.put(element.getName(), element.getValue());
			} catch (JSONException e) {
				log.error(e.getMessage());
			}
		}

		try {
			return true;
		} catch (Exception e) {
			log.error(e.getMessage(), e.getCause());
			throw new HandlerException("Audit Trail Exception ", AuditHandler.class, e);
		}
	}

}
