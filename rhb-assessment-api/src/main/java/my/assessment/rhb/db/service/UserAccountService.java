package my.assessment.rhb.db.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.mysema.query.jpa.impl.JPAQuery;

import lombok.extern.slf4j.Slf4j;
import my.assessment.rhb.db.model.QUserAccount;
import my.assessment.rhb.db.model.UserAccount;
import my.assessment.rhb.db.repository.UserAccountRepository;
import my.assessment.rhb.db.specification.UserAccountSpecification;
import my.assessment.rhb.db.specification.criteria.UserAccountCriteria;
import my.assessment.rhb.util.JsonOutput;

@Service
@Slf4j
public class UserAccountService implements BaseService<UserAccount> {
	
	@Autowired
	private UserAccountRepository repos;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public UserAccount findById(long id) {
		return repos.findOne(id);
	}

	@Override
	public UserAccount save(UserAccount record) {
		return repos.save(record);
	}
	
	@Override
	public Iterable<UserAccount> findAll() {
		return repos.findByLoginNameIsNotIn("sysoperator");
	}
	@Override
	public boolean delete(long id) {
		try {
			log.debug("UserAccountRepository.delete: Delete record with id {}", id);
			repos.delete(id);
			
			return true;
		} catch (IllegalArgumentException e) {
			log.error("UserAccountRepository.delete: Error on delete record: {} ", e.getMessage());
			throw new IllegalArgumentException("exception.technical.error");
		} catch (EmptyResultDataAccessException e) {
			log.error("UserAccountRepository.delete: Error on delete record: {} ", e.getMessage());
			throw new EmptyResultDataAccessException("exception.no.record.found", e.getExpectedSize());
		} catch (DataIntegrityViolationException e) {
			log.error("UserAccountRepository.delete: Error on delete record: {} ", e.getMessage());
			throw new DataIntegrityViolationException("exception.data.intigrity.constraint");
		}
	}

	@Override
	public boolean delete(UserAccount record) {
		return delete(record.getId());
	}

//	@Override
//	public Iterable<UserAccount> findAll() {
//		return repos.findAll();
//	}
	
	public List<UserAccount> findByUserRoleId(long userRoleId){
		return repos.findByUserRoleId(userRoleId);
	}
	
	public UserAccount findByLoginName(String loginName) {
		return repos.findByLoginName(loginName);
	}
	
	public UserAccount findByLoginNameAndEmailAddress(String loginName,String email) {
		return repos.findByLoginNameAndEmailAddress(loginName,email);
	}
	
	public JsonOutput<UserAccount> findByCriteria(JSONObject input, UserAccountCriteria criteria) {
		try {
			UserAccountSpecification specBuilder = new UserAccountSpecification(criteria);
			
			QUserAccount model = QUserAccount.userAccount;
			
			JPAQuery query = new JPAQuery(entityManager);
			query.from(model).where(specBuilder.build());
			
			JsonOutput<UserAccount> output = new JsonOutput<UserAccount>();
			
			long totalRecord = query.count();

			JSONObject page = input.has("page") ? input.getJSONObject("page") : null;
			
			if (page == null) {
				output.setData(query.orderBy(specBuilder.buildOrder(input)).list(model));
				output.setRecordsFiltered(totalRecord);
				output.setRecordsTotal(totalRecord);
				return output;
			}
			
			// if dont has filter
			if (!input.has("filters")) {
				output.setData(query.offset(page.getLong("from")).limit(page.getLong("size")).orderBy(specBuilder.buildOrder(input)).list(model));
				output.setRecordsFiltered(totalRecord);
			} else {
				JPAQuery filterQuery = new JPAQuery(entityManager);
				filterQuery.from(model)
					.where(specBuilder.buildFilter(specBuilder.build(), input)).orderBy(specBuilder.buildOrder(input));
				
				output.setData(filterQuery.offset(page.getLong("from")).limit(page.getLong("size")).list(model));
				output.setRecordsFiltered(filterQuery.count());
			}
			
			output.setRecordsTotal(totalRecord);
			return output;
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}
		return new JsonOutput<UserAccount>();
	}

}
