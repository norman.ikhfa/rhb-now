package my.assessment.rhb.db.specification.criteria;

import java.io.Serializable;

import lombok.Data;

@Data
public class PersonCriteria implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String firstName;
	
	private String lastName;
	
	private String email;
	
	public PersonCriteria(){
	}
	
	public PersonCriteria addFirstName(String firstName){
		setFirstName(firstName);
		return this;
	}
	
	public PersonCriteria addLastName(String lastName){
		setLastName(lastName);
		return this;
	}
	
	public PersonCriteria addEmail(String email){
		setEmail(email);
		return this;
	}
	
}
