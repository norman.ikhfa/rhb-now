package my.assessment.rhb.db.repository;

import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.data.repository.CrudRepository;

import my.assessment.rhb.db.model.UserAccount;

public interface UserAccountRepository extends CrudRepository<UserAccount, Long> {

	UserAccount findByLoginName(String loginName) throws DataAccessException;
	
	UserAccount findByLoginNameAndEmailAddress(String loginName,String email);
	
	List<UserAccount> findByUserRoleId(long userRoleId);;
	
	Iterable<UserAccount> findByLoginNameIsNotIn(String loginName);

}
