package my.assessment.rhb.db.repository;

import org.springframework.data.repository.CrudRepository;

import my.assessment.rhb.db.model.UserRole;

public interface UserRoleRepository extends CrudRepository<UserRole, Long> {

	UserRole findByCode(String roleCode);
	
	Iterable<UserRole> findByCodeIsNotIn(String roleCode);

}
