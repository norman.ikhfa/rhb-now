package my.assessment.rhb.db.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonView;

import lombok.Getter;
import lombok.Setter;
import my.assessment.rhb.util.JsonOutput;

@MappedSuperclass
public abstract class BaseEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Setter @Getter
	@JsonView(JsonOutput.View.class)
	private long id;
	
	@Setter @Getter
	@Column(name="created_by")
	@CreatedBy
	private String createdBy;
	
	@Setter @Getter
	@Column(name="updated_by")
	@LastModifiedBy
	private String updatedBy;
	
	@Setter @Getter
	@Column(name="created_date")
	@CreatedDate
	private Date createdDate;
	
	@Setter @Getter
	@Column(name="updated_date")
	@LastModifiedDate
	private Date updatedDate;
	
	
}
