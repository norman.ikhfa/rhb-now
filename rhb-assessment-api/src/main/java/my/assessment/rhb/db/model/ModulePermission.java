package my.assessment.rhb.db.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import my.assessment.rhb.constant.AccessLevel;
import my.assessment.rhb.converter.AccessLevelConverter;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Entity
@Table(name="module_permission")
@Data
@ToString(exclude="userRoles")
@EqualsAndHashCode(callSuper=false,exclude="userRoles")
public class ModulePermission extends BaseEntity {

	private static final long serialVersionUID = 1L;
	
	
	@Convert(converter = AccessLevelConverter.class)
	@Column(name="permission_name", columnDefinition="VARCHAR(45)")
	private AccessLevel permissionName;
	
	@ManyToOne
	@JoinColumn(name="module_id")
	@JsonBackReference
	private Module module;
	
	@ManyToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.DETACH})
	@JoinTable(name="role_permission", joinColumns=@JoinColumn(name="module_permission_id"), inverseJoinColumns=@JoinColumn(name="role_id"))
	@JsonBackReference
	private List<UserRole> userRoles;
	
	public ModulePermission() {
	}
	
	public ModulePermission(AccessLevel permissionName) {
		this.permissionName = permissionName;
	}
	
	public ModulePermission(AccessLevel permissionName, Module module) {
		this.permissionName = permissionName;
		this.module = module;
	}
}
