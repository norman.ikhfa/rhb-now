package my.assessment.rhb.db.specification;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.mysema.query.support.Expressions;
import com.mysema.query.types.Order;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.path.PathBuilder;
import com.mysema.query.types.path.StringPath;

import my.assessment.rhb.db.model.QUserRole;
import my.assessment.rhb.db.model.UserRole;
import my.assessment.rhb.db.specification.criteria.UserRoleCriteria;

public class UserRoleSpecification extends BaseSpecification {

	UserRoleCriteria input;

	public UserRoleSpecification(UserRoleCriteria input) {
		this.input = input;
	}

	@Override
	public BooleanExpression build() {
		QUserRole model = QUserRole.userRole;
		BooleanExpression result = model.eq(QUserRole.userRole);

		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public OrderSpecifier<String>[] buildOrder(JSONObject input) {
		PathBuilder<UserRole> path = new PathBuilder<UserRole>(UserRole.class, "userRole");

		List<OrderSpecifier<String>> specifiers = new ArrayList<>();

		try {
			if (input.has("sort")) {
				JSONObject sort = input.getJSONObject("sort");

				Order order = sort.getBoolean("reverse") == true ? Order.DESC : Order.ASC;

				specifiers.add(new OrderSpecifier<>(order, Expressions.path(String.class, path, sort.getString("by"))));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return specifiers.stream().toArray(OrderSpecifier[]::new);
	}

	@Override
	public BooleanExpression buildFilter(BooleanExpression predicate, JSONObject input) {

		try {
			if (input.has("filters")) {
				JSONArray filters = input.getJSONArray("filters");
				JSONObject obj = null;

				PathBuilder<UserRole> path = new PathBuilder<UserRole>(UserRole.class, "userRole");
				StringPath stringPath = null;

				for (int i = 0; i < filters.length(); i++) {
					obj = filters.getJSONObject(i);

					stringPath = Expressions.stringPath(path, obj.getString("property"));
					predicate = predicate.and(stringPath.containsIgnoreCase(obj.getString("value")));

				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return predicate;
	}

}
