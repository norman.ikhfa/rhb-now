package my.assessment.rhb.db.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonView;

import lombok.Data;
import lombok.EqualsAndHashCode;
import my.assessment.rhb.util.JsonOutput;

/**
 * The Class Person.
 * 
 * @author Norman Ikhfa
 */

@Entity
@Table(name = "person")
@Data
@EqualsAndHashCode(callSuper=false)
@JsonIgnoreProperties(value = { "handler", "hibernateLazyInitializer" })
public class Person extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Column(name="first_name", columnDefinition="VARCHAR(255)")
	@JsonView(JsonOutput.View.class)
	private String firstName;
	
	@Column(name="last_name", columnDefinition="VARCHAR(255)")
	@JsonView(JsonOutput.View.class)
	private String lastName;
	
	@Column(name="email", columnDefinition="VARCHAR(255)")
	@JsonView(JsonOutput.View.class)
	private String email;

}
