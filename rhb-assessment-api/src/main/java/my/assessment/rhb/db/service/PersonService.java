package my.assessment.rhb.db.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.mysema.query.jpa.impl.JPAQuery;

import lombok.extern.slf4j.Slf4j;
import my.assessment.rhb.db.model.Person;
import my.assessment.rhb.db.model.QPerson;
import my.assessment.rhb.db.repository.PersonRepository;
import my.assessment.rhb.db.specification.PersonSpecification;
import my.assessment.rhb.db.specification.criteria.PersonCriteria;
import my.assessment.rhb.util.JsonOutput;

@Service
@Slf4j
public class PersonService implements BaseService<Person> {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	private PersonRepository repos;

	@Override
	public Person findById(long id) {
		return repos.findOne(id);
	}
	
	@Override
	public Person save(Person record) {
		return repos.save(record);
	}

	@Override
	public boolean delete(long id) {
		try {
			log.debug("PersonService.delete: Delete record with id {}", id);
			repos.delete(id);
			
			return true;
		} catch (IllegalArgumentException e) {
			log.error("PersonService.delete: Error on delete record: {} ", e.getMessage());
			throw new IllegalArgumentException("exception.technical.error");
		} catch (EmptyResultDataAccessException e) {
			log.error("PersonService.delete: Error on delete record: {} ", e.getMessage());
			throw new EmptyResultDataAccessException("exception.no.record.found", e.getExpectedSize());
		} catch (DataIntegrityViolationException e) {
			log.error("PersonService.delete: Error on delete record: {} ", e.getMessage());
			throw new DataIntegrityViolationException("exception.data.intigrity.constraint");
		}
	}

	@Override
	public boolean delete(Person record) {
		return delete(record.getId());
	}
	
	@Override
	public List<Person> findAll() {
		Iterable<Person> iter = repos.findAll();
		List<Person> list = new ArrayList<Person>();
		iter.forEach(list::add);
		return list;
	}

	public JsonOutput<Person> findByCriteria(JSONObject input, PersonCriteria criteria) {
		try {
			
			PersonSpecification specBuilder = new PersonSpecification(criteria);
			
			QPerson model = QPerson.person;
			
			JPAQuery query = new JPAQuery(entityManager);
			query.from(model).where(specBuilder.build());
			
			JsonOutput<Person> output = new JsonOutput<Person>();
			
			long totalRecord = query.count();
			
			JSONObject page = input.has("page") ? input.getJSONObject("page") : null;
			
			if (page == null) {
				 
				output.setData(query.orderBy(specBuilder.buildOrder(input)).list(model));
				output.setRecordsFiltered(totalRecord);
				output.setRecordsTotal(totalRecord);
				return output;
			}
			
			// if dont has filter
			if (!input.has("filters")) {
				output.setData(query.offset(page.getLong("from")).limit(page.getLong("size")).orderBy(specBuilder.buildOrder(input)).list(model));
				output.setRecordsFiltered(totalRecord);
			} else {
				JPAQuery filterQuery = new JPAQuery(entityManager);
				filterQuery.from(model)
					.where(specBuilder.buildFilter(specBuilder.build(), input)).orderBy(specBuilder.buildOrder(input));
				
				output.setData(filterQuery.offset(page.getLong("from")).limit(page.getLong("size")).list(model));
				output.setRecordsFiltered(filterQuery.count());
			}
			
			output.setRecordsTotal(totalRecord);
			return output;
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}
		return new JsonOutput<Person>();
	}
	
	public List<Person> findByFirstName(String firstName) {
		return repos.findByFirstName(firstName);
	}
}
