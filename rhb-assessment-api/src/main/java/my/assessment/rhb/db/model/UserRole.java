package my.assessment.rhb.db.model;


import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.codehaus.jettison.json.JSONObject;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonView;

import lombok.Data;
import lombok.EqualsAndHashCode;
import my.assessment.rhb.util.JsonOutput;


@Entity
@Table(name = "user_role")
@Data
@EqualsAndHashCode(callSuper = false)
@JsonIgnoreProperties(value = { "handler", "hibernateLazyInitializer" })
public class UserRole extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "role_code", columnDefinition = "VARCHAR(50)")
	@JsonView(JsonOutput.View.class)
	private String code;

	@Column(name = "role_name", columnDefinition = "VARCHAR(255)")
	@JsonView(JsonOutput.View.class)
	private String name;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "role_permission", joinColumns = @JoinColumn(name = "role_id"), inverseJoinColumns = @JoinColumn(name = "module_permission_id"))
	private List<ModulePermission> permissions;


	public UserRole() {
	}


	public UserRole(String code, String name) {
		super();
		this.code = code;
		this.name = name;
	}


	public String toString() {
		try {
			JSONObject obj = new JSONObject();
			obj.put("id", getId());
			obj.put("code", getCode());
			obj.put("name", getName());
			return obj.toString();
		} catch (Exception e) {
		}
		return super.toString();
	}

}
