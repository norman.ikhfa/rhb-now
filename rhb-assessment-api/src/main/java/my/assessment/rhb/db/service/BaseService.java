package my.assessment.rhb.db.service;

public interface BaseService<T> {

	/**
	 * Find by id.
	 *
	 * @param id the id
	 * @return the t
	 */
	T findById(long id);
	
	/**
	 * Save record.
	 *
	 * @param record the record
	 * @return the t
	 */
	T save(T record);
	
	/**
	 * Delete record.
	 *
	 * @param id the id
	 * @return true, if successful
	 */
	boolean delete(long id);
	
	/**
	 * Delete record.
	 *
	 * @param record the record
	 * @return true, if successful
	 */
	boolean delete(T record);
	
	/**
	 * Find all record.
	 *
	 * @return the iterable
	 */
	Iterable<T> findAll();

}
