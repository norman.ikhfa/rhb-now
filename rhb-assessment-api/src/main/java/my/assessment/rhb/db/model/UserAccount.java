package my.assessment.rhb.db.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonView;

import lombok.Data;
import lombok.EqualsAndHashCode;
import my.assessment.rhb.util.JsonOutput;

@Entity
@Table(name = "user_account")
@Data
@EqualsAndHashCode(callSuper=false)
@JsonIgnoreProperties(value = { "handler", "hibernateLazyInitializer" })
public class UserAccount extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Column(name="login_name",columnDefinition="VARCHAR(50)")
	@JsonView(JsonOutput.View.class)
	private String loginName;
	
	@Column(name="user_pwd",columnDefinition="VARCHAR(255)")
	@JsonView(JsonOutput.View.class)
	private String userPassword;
	
	@Temporal(value=TemporalType.TIMESTAMP)
	@Column(name="last_login")
	private Date lastLogin;
	
	@Column(name="full_name",columnDefinition="VARCHAR(150)")
	private String fullName;
	
	@Column(name="email_address",columnDefinition="VARCHAR(200)")
	private String emailAddress;
	
	@Column(name="phone_number",columnDefinition="VARCHAR(15)")
	private String phoneNumber;
	
	@Column(name="address",columnDefinition="TEXT")
	private String address;
	
	@Column(name="postcode",columnDefinition="VARCHAR(5)")
	private String postcode;
	
	@Column(name="mobile_user", columnDefinition="BOOLEAN DEFAULT TRUE")
	private boolean mobileUser;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="role_id")
	@JsonView(JsonOutput.View.class)
	private UserRole userRole;
	
	@Column(name="ca_info",columnDefinition="VARCHAR(5)")
	@JsonView(JsonOutput.View.class)
	private String caInfo;
	
	public UserAccount() {
	}
	
	public UserAccount(String loginName, String userPassword, String fullName, String email) {
		super();
		this.loginName = loginName;
		this.userPassword = userPassword;
		this.fullName = fullName;
		this.emailAddress = email;
	}

}
