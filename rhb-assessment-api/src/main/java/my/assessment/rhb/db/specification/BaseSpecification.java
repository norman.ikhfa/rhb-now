package my.assessment.rhb.db.specification;

import org.codehaus.jettison.json.JSONObject;

import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.expr.BooleanExpression;

public abstract class BaseSpecification {

	abstract public BooleanExpression build();
	
	abstract public OrderSpecifier<String>[] buildOrder(JSONObject input);
	
	abstract public BooleanExpression buildFilter(BooleanExpression predicate, JSONObject input);
}
