package my.assessment.rhb.db.specification.criteria;

import java.io.Serializable;

import lombok.Data;

@Data
public class UserAccountCriteria implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private long userGroupId;
	
	public UserAccountCriteria(){
	}
	
	public UserAccountCriteria addUserGroupId(long userGroupId){
		setUserGroupId(userGroupId);
		return this;
	}
	

}
