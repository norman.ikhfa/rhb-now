package my.assessment.rhb.db.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import my.assessment.rhb.constant.AccessLevel;
import my.assessment.rhb.db.model.Module;
import my.assessment.rhb.db.model.ModulePermission;
import my.assessment.rhb.db.model.UserRole;

public interface ModulePermissionRepository extends CrudRepository<ModulePermission, Long> {

	ModulePermission findByModuleAndPermissionNameAndUserRolesIn(Module module, AccessLevel permissionName, List<UserRole> userRoles);
	
	ModulePermission findByPermissionNameAndModule(AccessLevel permissionName, Module module);
	
	ModulePermission findByModuleControllerNameAndPermissionNameAndUserRolesIn(String controllerName, AccessLevel permissionName, List<UserRole> userRoles);
	
	List<ModulePermission> findByModuleControllerNameAndUserRolesIn(String controllerName, List<UserRole> userRoles);
	
}
