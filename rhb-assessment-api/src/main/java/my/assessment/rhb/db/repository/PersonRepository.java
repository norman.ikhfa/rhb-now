package my.assessment.rhb.db.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import my.assessment.rhb.db.model.Person;

public interface PersonRepository extends CrudRepository<Person, Long> {

	List<Person> findByFirstName(String firstName);

}
