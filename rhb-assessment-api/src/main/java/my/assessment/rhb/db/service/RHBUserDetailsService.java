package my.assessment.rhb.db.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import my.assessment.rhb.db.model.RHBUser;
import my.assessment.rhb.db.model.UserAccount;
import my.assessment.rhb.db.repository.UserAccountRepository;

@Service
@Transactional
public class RHBUserDetailsService implements IUserDetailsService {
	
	@Autowired
	private UserAccountRepository userAcctRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserAccount account = userAcctRepository.findByLoginName(username);
		
        if (account == null) {
            throw new UsernameNotFoundException(String.format("No user found with loginName '%s'.", username));
        }
        
        return new RHBUser(account);
	}	

}
