package my.assessment.rhb.db.specification;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.mysema.query.support.Expressions;
import com.mysema.query.types.Order;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.path.PathBuilder;
import com.mysema.query.types.path.StringPath;

import my.assessment.rhb.db.model.Person;
import my.assessment.rhb.db.model.QPerson;
import my.assessment.rhb.db.specification.criteria.PersonCriteria;


public class PersonSpecification extends BaseSpecification {
	
	PersonCriteria input;
	
	public PersonSpecification(PersonCriteria input) {
		this.input = input;
	}
	
	@Override
	public BooleanExpression build() {
		QPerson model = QPerson.person;
		BooleanExpression result = model.eq(QPerson.person);
		
		if (input.getFirstName() != null) {
			result = result.and(model.firstName.like(input.getFirstName()));
		}
		
		if (input.getLastName() != null) {
			result = result.and(model.lastName.like(input.getLastName()));
		}
		
		if (input.getEmail() != null) {
			result = result.and(model.email.like(input.getEmail()));
		}
		
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public OrderSpecifier<String>[] buildOrder(JSONObject input) {
		PathBuilder<Person> path = new PathBuilder<Person>(Person.class, "person");
		
		List<OrderSpecifier<String>> specifiers = new ArrayList<>();
		
		try {
			if (input.has("sort")) {
				JSONObject sort = input.getJSONObject("sort");
				
				Order order = sort.getBoolean("reverse") == true ? Order.DESC : Order.ASC;
				
				specifiers.add(new OrderSpecifier<>(order, Expressions.path(String.class, path, sort.getString("by") )) );
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return specifiers.stream().toArray(OrderSpecifier[]::new);
	}

	@Override
	public BooleanExpression buildFilter(BooleanExpression predicate, JSONObject input) {
		
		try {
			if (input.has("filters")) {
				JSONArray filters = input.getJSONArray("filters");
				JSONObject obj = null;
				
				PathBuilder<Person> path = new PathBuilder<Person>(Person.class, "person");
				StringPath stringPath = null;
				
				for (int i =0; i < filters.length(); i++) {
					obj = filters.getJSONObject(i);
					
					stringPath = Expressions.stringPath(path,  obj.getString("property"));
					predicate = predicate.and(stringPath.containsIgnoreCase(obj.getString("value")));

				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		

		return predicate;
	}

}
