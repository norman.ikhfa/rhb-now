package my.assessment.rhb.db.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import my.assessment.rhb.db.model.Module;
import my.assessment.rhb.db.repository.ModuleRepository;

@Service
@Slf4j
public class ModuleService implements BaseService<Module> {

	@Autowired
	private ModuleRepository repos;
	
	@Override
	public Module findById(long id) {
		return repos.findOne(id);
	}

	@Override
	public Module save(Module record) {
		return repos.save(record);
	}

	@Override
	public boolean delete(long id) {
		try {
			log.debug("ModuleService.delete: Delete record with id {}", id);
			repos.delete(id);
			
			return true;
		} catch (IllegalArgumentException e) {
			log.error("ModuleService.delete: Error on delete record: {} ", e.getMessage());
			throw new IllegalArgumentException("exception.technical.error");
		} catch (EmptyResultDataAccessException e) {
			log.error("ModuleService.delete: Error on delete record: {} ", e.getMessage());
			throw new EmptyResultDataAccessException("exception.no.record.found", e.getExpectedSize());
		} catch (DataIntegrityViolationException e) {
			log.error("ModuleService.delete: Error on delete record: {} ", e.getMessage());
			throw new DataIntegrityViolationException("exception.data.intigrity.constraint");
		}
	}

	@Override
	public boolean delete(Module record) {
		return delete(record.getId());
	}

	@Override
	public Iterable<Module> findAll() {
		return repos.findAll();
	}
	
	public Module findByModuleName(String moduleName) {
		return repos.findByModuleName(moduleName);
	}
	
//	public List<Module> findByApplicationType(String applicationTypeName) {
//		return repos.findByApplicationTypeNameOrderByModuleName(applicationTypeName);
//	}
	
//	public Module findByControllerNameAndApplicationType(String controllerName, String applicationTypeName) {
//		return repos.findByControllerNameAndApplicationTypeName(controllerName, applicationTypeName);
//	}
	
	public Module findByControllerName(String controllerName) {
		return repos.findByControllerName(controllerName);
	}

}
