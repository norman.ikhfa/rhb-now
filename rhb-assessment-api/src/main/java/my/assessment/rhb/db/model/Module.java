package my.assessment.rhb.db.model;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;


@Entity
@Table(name = "core_module")
@Data
@ToString(exclude = "modulePermissions")
@EqualsAndHashCode(callSuper = false)
@JsonIgnoreProperties(value = { "handler", "hibernateLazyInitializer" })
public class Module extends BaseEntity {

	private static final long serialVersionUID = 1L;

	@Column(name = "module_name")
	private String moduleName;

	@Column(name = "controller_name")
	private String controllerName;

	@Column(name = "root_url")
	private String rootUrl;

	@Column(name = "core_type")
	private boolean coreType;

	@Column(columnDefinition = "TEXT")
	private String remark;

	@OneToMany(mappedBy = "module", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
	private List<ModulePermission> modulePermissions = new ArrayList<ModulePermission>(0);


	public Module() {
	}


	public Module(String moduleName, String rootUrl, boolean coreType, String remark) {
		super();
		this.moduleName = moduleName;
		this.rootUrl = rootUrl;
		this.coreType = coreType;
		this.remark = remark;
	}

}
