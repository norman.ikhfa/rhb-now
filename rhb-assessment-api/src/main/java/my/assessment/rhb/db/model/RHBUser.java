package my.assessment.rhb.db.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class RHBUser implements UserDetails {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private UserAccount account;
	private Collection<? extends GrantedAuthority> authorities;
	
	public RHBUser(UserAccount account) {
		this.account = account;
		this.authorities = mapToGrantedAuthorities(this.account.getUserRole());
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return this.authorities;
	}

	@Override
	public String getPassword() {
		return account.getUserPassword();
	}

	@Override
	public String getUsername() {
		return account.getLoginName();
	}
	
	public UserAccount getUserAccount() {
		return account;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}
	
	private static List<GrantedAuthority> mapToGrantedAuthorities(UserRole userRole) {
    	
    	List<UserRole> authorities = new ArrayList<UserRole>();
    	authorities.add(userRole);
    	
        return authorities.stream()
                .map(authority -> new SimpleGrantedAuthority(authority.getCode()))
                .collect(Collectors.toList());
    }

}
