package my.assessment.rhb.db.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import my.assessment.rhb.db.model.Module;

public interface ModuleRepository extends JpaRepository<Module, Long> {
	
	Module findByModuleName(String moduleName);
	
	Module findByControllerName(String controllerName);
	
}
