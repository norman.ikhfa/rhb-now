package my.assessment.rhb.db.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import my.assessment.rhb.constant.AccessLevel;
import my.assessment.rhb.db.model.Module;
import my.assessment.rhb.db.model.ModulePermission;
import my.assessment.rhb.db.model.UserRole;
import my.assessment.rhb.db.repository.ModulePermissionRepository;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class PermissionService {
	
	@Autowired
	private ModulePermissionRepository modulePermissionRepository;
	
	public ModulePermission saveModulePermission(ModulePermission modulePermission) {
		return modulePermissionRepository.save(modulePermission);
	}
	
	
	public void deleteModulePermission(ModulePermission modulePermission) {
		modulePermissionRepository.delete(modulePermission);
	}
	
	public ModulePermission getModulePermissionById(long id) {
		return modulePermissionRepository.findOne(id);
	}
	
	public ModulePermission getByPermissionNameAndModule(AccessLevel permissionName, Module module) {
		return modulePermissionRepository.findByPermissionNameAndModule(permissionName, module);
	}
	
	public ModulePermission getModulePermission(String controllerName, String permissionName, List<UserRole> userRoles) {
		ModulePermission mp = null;
		try {
			mp = modulePermissionRepository.findByModuleControllerNameAndPermissionNameAndUserRolesIn(controllerName, AccessLevel.valueOf(permissionName), userRoles);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return mp;
	}
	
	public ModulePermission getModulePermission(Module module, String permissionName, List<UserRole> userRoles) {
		ModulePermission mp = null;
		try {
			mp = modulePermissionRepository.findByModuleAndPermissionNameAndUserRolesIn(module, AccessLevel.valueOf(permissionName), userRoles);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return mp;
	}
	
	public boolean hasPermission(Module module, String permissionName, List<UserRole> userRoles) {
		return getModulePermission(module, permissionName, userRoles) != null;
	}
	
	public boolean hasPermission(String controllerName, String permissionName, List<UserRole> userRoles) {
		return getModulePermission(controllerName, permissionName, userRoles) != null;
	}
	
	public List<ModulePermission> getModulePermission(String controllerName, List<UserRole> userRoles) {
		List<ModulePermission> list = null;
		try {
			list = modulePermissionRepository.findByModuleControllerNameAndUserRolesIn(controllerName, userRoles);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return list;
	}

	public boolean hasPermission(AccessLevel accessLevel, Module module, UserRole userRole) {
		
		// allow for sysoperator
		if (userRole.getCode().equals("sysoperator")) {
			return true;
		}
		
		List<UserRole> roles = new ArrayList<UserRole>();
		roles.add(userRole);
		
		ModulePermission mp = null;
		try {
			mp = modulePermissionRepository.findByModuleAndPermissionNameAndUserRolesIn(module, accessLevel, roles);
			return mp != null;
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		
		return false;
	}
}
