package my.assessment.rhb.db.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.mysema.query.jpa.impl.JPAQuery;

import lombok.extern.slf4j.Slf4j;
import my.assessment.rhb.db.model.QUserRole;
import my.assessment.rhb.db.model.UserRole;
import my.assessment.rhb.db.repository.UserRoleRepository;
import my.assessment.rhb.db.specification.UserRoleSpecification;
import my.assessment.rhb.db.specification.criteria.UserRoleCriteria;
import my.assessment.rhb.util.JsonOutput;

@Service
@Slf4j
public class UserRoleService implements BaseService<UserRole> {
	
	@Autowired
	private UserRoleRepository repos;
	
	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public UserRole findById(long id) {
		return repos.findOne(id);
	}

	@Override
	public UserRole save(UserRole record) {
		return repos.save(record);
	}

	@Override
	public boolean delete(long id) {
		try {
			log.debug("UserRoleService.delete: Delete record with id {}", id);
			repos.delete(id);
			
			return true;
		} catch (IllegalArgumentException e) {
			log.error("UserRoleService.delete: Error on delete record: {} ", e.getMessage());
			throw new IllegalArgumentException("exception.technical.error");
		} catch (EmptyResultDataAccessException e) {
			log.error("UserRoleService.delete: Error on delete record: {} ", e.getMessage());
			throw new EmptyResultDataAccessException("exception.no.record.found", e.getExpectedSize());
		} catch (DataIntegrityViolationException e) {
			log.error("UserRoleService.delete: Error on delete record: {} ", e.getMessage());
			throw new DataIntegrityViolationException("exception.data.intigrity.constraint");
		}
	}

	@Override
	public boolean delete(UserRole record) {
		return delete(record.getId());
	}

	@Override
	public Iterable<UserRole> findAll() {
		return repos.findByCodeIsNotIn("sysoperator");
	}
	
	/**
	 * Gets the all user role code.
	 *
	 * @return the all user role code
	 */
	public String[] getAllUserRoleCode() {
		List<String> roleId = new ArrayList<String>();
		
		for (UserRole role: findAll()) {
			roleId.add(role.getCode());
		}
		
		return roleId.stream().toArray(String[]::new);
	}
	
	/**
	 * Find by role code.
	 *
	 * @param roleCode the role code
	 * @return the user role
	 */
	public UserRole findByCode(String roleCode) {
		return  repos.findByCode(roleCode);			

	}

	public JsonOutput<UserRole> findByCriteria(JSONObject input, UserRoleCriteria criteria) {
		try {
			UserRoleSpecification specBuilder = new UserRoleSpecification(criteria);
			
			QUserRole model = QUserRole.userRole;
			
			JPAQuery query = new JPAQuery(entityManager);
			query.from(model).where(specBuilder.build());
			
			JsonOutput<UserRole> output = new JsonOutput<UserRole>();
			
			long totalRecord = query.count();
			
			JSONObject page = input.has("page") ? input.getJSONObject("page") : null;
			
			if (page == null) {
				output.setData(query.orderBy(specBuilder.buildOrder(input)).list(model));
				output.setRecordsFiltered(totalRecord);
				output.setRecordsTotal(totalRecord);
				return output;
			}
			
			// if dont has filter
			if (!input.has("filters")) {
				output.setData(query.offset(page.getLong("from")).limit(page.getLong("size")).orderBy(specBuilder.buildOrder(input)).list(model));
				output.setRecordsFiltered(totalRecord);
			} else {
				JPAQuery filterQuery = new JPAQuery(entityManager);
				filterQuery.from(model)
					.where(specBuilder.buildFilter(specBuilder.build(), input)).orderBy(specBuilder.buildOrder(input));
				
				output.setData(filterQuery.offset(page.getLong("from")).limit(page.getLong("size")).list(model));
				output.setRecordsFiltered(filterQuery.count());
			}
			
			output.setRecordsTotal(totalRecord);
			return output;
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}
		return new JsonOutput<UserRole>();
	}

}
