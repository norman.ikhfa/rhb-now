package my.assessment.rhb.component;


import java.lang.annotation.Annotation;
import java.util.Arrays;
import java.util.Date;
import java.util.Set;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;
import my.assessment.rhb.constant.AccessLevel;
import my.assessment.rhb.db.model.Module;
import my.assessment.rhb.db.model.ModulePermission;
import my.assessment.rhb.db.service.ModuleService;
import my.assessment.rhb.db.service.PermissionService;
import my.assessment.rhb.util.RHBModule;


@Component
@Slf4j
@EnableConfigurationProperties
public class ModuleInit implements ApplicationListener<ContextRefreshedEvent> {

	@Value("${rhb.initialize.module}")
	private boolean toInitializeModule;

	private boolean alreadySetup = false;

	@Autowired
	private PermissionService permissionService;

	@Autowired
	private ModuleService service;


	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		if (alreadySetup) {
			return;
		}

		if (!toInitializeModule) {
			return;
		}

		log.debug("initialize module setup...");
		ClassPathScanningCandidateComponentProvider provider = createComponentScanner(RHBModule.class);
		Set<BeanDefinition> beans = provider.findCandidateComponents("my.assessment.rhb.rest.module");

		for (BeanDefinition beanDef : beans) {
			Class<?> cls;
			try {
				cls = Class.forName(beanDef.getBeanClassName());

				if (cls.isAnnotationPresent(RHBModule.class)) {
					log.debug("found class with module defined: " + cls.getSimpleName());
					Annotation annotation = cls.getAnnotation(RHBModule.class);
					RHBModule rhbModule = (RHBModule) annotation;

					try {
						JSONObject obj = new JSONObject();
						obj.put("moduleName", rhbModule.moduleName());
						obj.put("coreType", rhbModule.coreType());
						obj.put("rootUrl", rhbModule.rootUrl());
						obj.put("controllerName", cls.getSimpleName());
						obj.put("accessLevel", Arrays.asList(rhbModule.accessLevel()));

						JSONArray accessLevels = obj.getJSONArray("accessLevel");

						Module module = null;
						if (obj.has("moduleName")) {
							module = service.findByModuleName(obj.getString("moduleName"));
							// ApplicationType appType =
							// appTypeService.findByName(objData.getString("applicationType"));
							if (module != null) {
								// update
								module.setUpdatedBy("SYSTEM");
								module.setUpdatedDate(new Date());
							} else {
								// add
								module = new Module();
								module.setCreatedDate(new Date());
								module.setCreatedBy("SYSTEM");
							}

							// module.setApplicationType(appType);
							module.setCoreType(obj.getBoolean("coreType"));
							module.setModuleName(obj.getString("moduleName"));
							module.setControllerName(obj.getString("controllerName"));
							module.setRootUrl(obj.getString("rootUrl"));
							module = service.save(module);

							// module access level
							ModulePermission permission = null;
							for (int i = 0; i < accessLevels.length(); i++) {
								permission = permissionService.getByPermissionNameAndModule(
										AccessLevel.valueOf(accessLevels.getString(i)), module);
								if (permission == null) {
									permission = new ModulePermission();
									permission.setModule(module);
									permission.setPermissionName(
											AccessLevel.valueOf(accessLevels.getString(i)));
									permission.setCreatedDate(new Date());
									permission.setCreatedBy("SYSTEM");
									permissionService.saveModulePermission(permission);
								}
							}
						}

						// RestTemplate restTemplate = new RestTemplate();
						// String url = adminApiUrl + "/module/update";
						// HttpEntity<String> request = new
						// HttpEntity<>(new
						// String(Base64.encode(obj.toString().getBytes())));
						// ResponseEntity<String> rest =
						// restTemplate.postForEntity(url, request,
						// String.class);
						//
						// if (rest.getStatusCode().name().equals("OK")) {
						//
						// }
					} catch (Exception e) {
						e.printStackTrace();
					}

				}

			} catch (ClassNotFoundException e1) {
			}
		}

		alreadySetup = true;

	}


	private ClassPathScanningCandidateComponentProvider createComponentScanner(Class<? extends Annotation> clazz) {
		// Don't pull default filters (@Component, etc.):
		ClassPathScanningCandidateComponentProvider provider = new ClassPathScanningCandidateComponentProvider(false);
		provider.addIncludeFilter(new AnnotationTypeFilter(clazz));
		return provider;
	}

}
