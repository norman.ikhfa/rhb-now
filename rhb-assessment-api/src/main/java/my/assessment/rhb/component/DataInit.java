package my.assessment.rhb.component;


import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;
import my.assessment.rhb.db.model.UserAccount;
import my.assessment.rhb.db.model.UserRole;
import my.assessment.rhb.db.service.UserAccountService;
import my.assessment.rhb.db.service.UserRoleService;


@Component
@Slf4j
@EnableConfigurationProperties
public class DataInit implements ApplicationListener<ContextRefreshedEvent> {

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private UserAccountService userAcctService;

	@Autowired
	private UserRoleService userRoleService;

	private boolean alreadySetup = false;


	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		log.debug("initialize basic data");

		if (alreadySetup) {
			return;
		}

		// user role for system admin
		UserRole role = userRoleService.findByCode("sysoperator");
		if (role == null) {
			role = new UserRole("sysoperator", "Pengendali Sistem");
			role.setCreatedBy("SYSTEM");
			role.setCreatedDate(new Date());
			role = userRoleService.save(role);
		}

		// user account n profile
		UserAccount userAcct = userAcctService.findByLoginName("sysoperator");
		if (userAcct == null) {
			userAcct = new UserAccount("sysoperator", passwordEncoder.encode("password"), "Sysoperator",
					"normanikhfa@yahoo.com.my");
			userAcct.setUserRole(role);
			userAcct.setCreatedBy("SYSTEM");
			userAcct.setCreatedDate(new Date());
			userAcctService.save(userAcct);

		}

		alreadySetup = true;
	}

}
