package my.assessment.rhb.security;

import java.io.IOException;
import java.io.Serializable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

@Component
public class RHBAuthenticationEntryPoint implements AuthenticationEntryPoint, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException authException) throws IOException {
		
		// This is invoked when user tries to access a secured REST resource
		// without supplying any credentials
		// We should just send a 401 Unauthorized response because there is no
		// 'login page' to redirect to
		if (authException instanceof UsernameNotFoundException || authException instanceof BadCredentialsException) {
			response.sendError(HttpServletResponse.SC_NOT_ACCEPTABLE, "ID Pengguna atau Katalaluan salah");
		} else {
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Capaian tidak dibenarkan");
		}

	}

}
