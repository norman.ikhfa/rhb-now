package my.assessment.rhb.security;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class RHBAuthenticationRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String loginName;
    private String password;

    public RHBAuthenticationRequest() {
        super();
    }

    public RHBAuthenticationRequest(String loginName, String password) {
        this.setLoginName(loginName);
        this.setPassword(password);
    }

}
