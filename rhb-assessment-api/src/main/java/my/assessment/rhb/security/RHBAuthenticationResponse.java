package my.assessment.rhb.security;

import java.io.Serializable;
import java.util.List;

public class RHBAuthenticationResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final String token;
	private final long id; 
	private final String fullName;
	private final List<String> userRole;

    public RHBAuthenticationResponse(String token, long accountId, String fullName, List<String> userRole) {
        this.token = token;
        this.id = accountId;
        this.fullName = fullName;
        this.userRole = userRole;
    }

    public String getToken() {
        return this.token;
    }

	public long getId() {
		return id;
	}
	
	public String getFullName() {
		return fullName;
	}
	
	public List<String> getUserRole() {
		return userRole;
	}

}
