<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<parent>
		<groupId>my.assessment.rhb</groupId>
		<artifactId>rhb-now</artifactId>
		<version>0.0.1-SNAPSHOT</version>
	</parent>

	<artifactId>rhb-assessment-api</artifactId>
	<packaging>war</packaging>
	<name>RHB Assessment API</name>

	<dependencies>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-security</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-mobile</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-data-rest</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-thymeleaf</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-mail</artifactId>
		</dependency>

		<dependency>
			<groupId>com.zaxxer</groupId>
			<artifactId>HikariCP</artifactId>
		</dependency>
		<!-- database fixtures -->
		<dependency>
			<groupId>org.liquibase</groupId>
			<artifactId>liquibase-core</artifactId>
		</dependency>

		<dependency>
			<groupId>io.jsonwebtoken</groupId>
			<artifactId>jjwt</artifactId>
			<version>0.7.0</version>
		</dependency>



		<dependency>
			<groupId>commons-codec</groupId>
			<artifactId>commons-codec</artifactId>
		</dependency>

		<!-- jasper report -->
		<dependency>
			<groupId>net.sf.jasperreports</groupId>
			<artifactId>jasperreports</artifactId>
			<version>6.4.0</version>
			<exclusions>
				<exclusion>
					<groupId>*</groupId>
					<artifactId>*</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
			<groupId>commons-beanutils</groupId>
			<artifactId>commons-beanutils</artifactId>
		</dependency>
		<dependency>
			<groupId>commons-digester</groupId>
			<artifactId>commons-digester</artifactId>
		</dependency>
		<dependency>
			<groupId>commons-lang</groupId>
			<artifactId>commons-lang</artifactId>
			<version>2.6</version>
		</dependency>
		<dependency>
			<groupId>org.apache.poi</groupId>
			<artifactId>poi</artifactId>
			<version>3.16</version>
		</dependency>
		
		<dependency>
			<groupId>com.mysema.querydsl</groupId>
			<artifactId>querydsl-core</artifactId>
			<version>3.6.0</version>
		</dependency>
		<dependency>
			<groupId>com.mysema.querydsl</groupId>
			<artifactId>querydsl-apt</artifactId>
			<version>3.6.0</version>
		</dependency>
		<dependency>
			<groupId>com.mysema.querydsl</groupId>
			<artifactId>querydsl-jpa</artifactId>
			<version>3.6.0</version>
		</dependency>
		
		<dependency>
			<groupId>my.assessment.rhb</groupId>
			<artifactId>rhb-core</artifactId>
			<version>${project.version}</version>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-devtools</artifactId>
			<scope>runtime</scope>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-tomcat</artifactId>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
		</dependency>
	</dependencies>
	
	<build>
		<finalName>rhb_assessment_api</finalName>
		<plugins>
			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
			</plugin>

			<plugin>
				<!-- Set timestamp in a variable so it can be used for generated files 
					by liquibase in different goals -->
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>buildnumber-maven-plugin</artifactId>
				<version>1.2</version>
				<configuration>
					<timestampFormat>yyyyMMdd_HHmmss</timestampFormat>
				</configuration>
				<executions>
					<execution>
						<phase>validate</phase>
						<goals>
							<goal>create-timestamp</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			
			<plugin>
				<groupId>com.mysema.maven</groupId>
				<artifactId>apt-maven-plugin</artifactId>
				<version>1.1.3</version>
				<executions>
					<execution>
						<goals>
							<goal>process</goal>
						</goals>
						<configuration>
							<outputDirectory>target/generated-sources/java</outputDirectory>
							<processor>com.mysema.query.apt.jpa.JPAAnnotationProcessor</processor>
							<options>
								<querydsl.entityAccessors>true</querydsl.entityAccessors>
							</options>
						</configuration>
					</execution>
				</executions>
			</plugin>
		</plugins>

		<pluginManagement>
			<plugins>
				<!--This plugin's configuration is used to store Eclipse m2e settings 
					only. It has no influence on the Maven build itself. -->
				<plugin>
					<groupId>org.eclipse.m2e</groupId>
					<artifactId>lifecycle-mapping</artifactId>
					<version>1.0.0</version>
					<configuration>
						<lifecycleMappingMetadata>
							<pluginExecutions>
								<pluginExecution>
									<pluginExecutionFilter>
										<groupId>org.codehaus.mojo</groupId>
										<artifactId>buildnumber-maven-plugin</artifactId>
										<versionRange>[1.2,)</versionRange>
										<goals>
											<goal>create-timestamp</goal>
										</goals>
									</pluginExecutionFilter>
									<action>
										<ignore></ignore>
									</action>
								</pluginExecution>
							</pluginExecutions>
						</lifecycleMappingMetadata>
					</configuration>
				</plugin>
			</plugins>
		</pluginManagement>
	</build>

	<profiles>
		<!-- ============================================================================================ -->
		<!-- Generate database diff with liquibase, based on previously registered 
			liquibase db changelog -->
		<!-- refer to http://www.baeldung.com/liquibase-refactor-schema-of-java-app -->
		<!-- ============================================================================================ -->
		<profile>
			<id>db-diff</id>
			<build>
				<plugins>
					<plugin>
						<groupId>org.liquibase</groupId>
						<artifactId>liquibase-maven-plugin</artifactId>
						<version>3.4.1</version>
						<configuration>
							<!-- <propertyFile>db/liquibase.properties</propertyFile> -->
							<changeLogFile>db/db_changelog.xml</changeLogFile>
							<diffChangeLogFile>src/main/resources/db/schema/db-${timestamp}.xml</diffChangeLogFile>
							<logging>info</logging>
							<changeSetAuthor>Norman</changeSetAuthor>
							<driver>com.mysql.jdbc.Driver</driver>
							<url>jdbc:mysql://localhost/_rhb_assessment_v1</url>
							<referenceUrl>hibernate:spring:my.assessment.rhb.db.model?dialect=org.hibernate.dialect.MySQL5InnoDBDialect</referenceUrl>
							<username>root</username>
							<password>malaysia</password>
						</configuration>
						<executions>
							<execution>
								<id>generate-db-prev</id>
								<phase>process-resources</phase>
								<goals>
									<goal>generateChangeLog</goal>
								</goals>
								<configuration>
									<dropFirst>true</dropFirst>
								</configuration>
							</execution> 
							<execution>
								<id>generate-db-diff</id>
								<phase>process-test-resources</phase>
								<goals>
									<!-- <goal>generateChangeLog</goal>
									<goal>dropAll</goal> -->
									<goal>diff</goal>
								</goals>
								<configuration>
						           <systemProperties>
						               <user.name>Norman Ikhfa</user.name>
						           </systemProperties>
						         </configuration>
							</execution>
							<execution>
								<id>drop-db</id>
								<phase>pre-clean</phase>
								<goals>
									<goal>dropAll</goal>
								</goals>
							</execution>
							<execution>
								<id>generate-db-data</id>
								<phase>generate-test-resources</phase>
								<goals>
									<goal>generateChangeLog</goal>
								</goals>
								<configuration>
									<diffTypes>data</diffTypes>
									<dataDir>src/main/resources/db/data</dataDir>
								</configuration>
							</execution>
						</executions>
						<dependencies>
							<dependency>
								<groupId>org.liquibase.ext</groupId>
								<artifactId>liquibase-hibernate4</artifactId>
								<version>3.5</version>
							</dependency>
							<dependency>
								<groupId>org.springframework</groupId>
								<artifactId>spring-beans</artifactId>
								<version>4.2.6.RELEASE</version>
							</dependency>
							<dependency>
								<groupId>org.springframework.data</groupId>
								<artifactId>spring-data-jpa</artifactId>
								<version>1.9.4.RELEASE</version>
							</dependency>
						</dependencies>
					</plugin>
				</plugins>
			</build>
		</profile>
	</profiles>
</project>