package my.assessment.rhb.converter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import my.assessment.rhb.constant.AuditType;

@Converter
public class AuditTypeConverter implements AttributeConverter<AuditType, String> {

	@Override
	public String convertToDatabaseColumn(AuditType attribute) {
		switch (attribute) {
		case SECURITY:
			return "SECURITY";
		case EMAIL:
			return "EMAIL";
		case PERSON:
			return "PERSON";
		case USERACCOUNT:
			return "USERACCOUNT";
		default:
			throw new IllegalArgumentException("Unknown" + attribute);
		}
	}

	@Override
	public AuditType convertToEntityAttribute(String dbData) {
		switch (dbData) {
		case "SECURITY":
			return AuditType.SECURITY;
		case "EMAIL":
			return AuditType.EMAIL;
		case "PERSON":
			return AuditType.PERSON;
		case "USERACCOUNT":
			return AuditType.USERACCOUNT;
		default:
			throw new IllegalArgumentException("Unknown" + dbData);
		}
	}

}
