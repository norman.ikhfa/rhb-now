package my.assessment.rhb.converter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import my.assessment.rhb.constant.AccessLevel;

@Converter
public class AccessLevelConverter implements AttributeConverter<AccessLevel, String> {

	@Override
	public String convertToDatabaseColumn(AccessLevel attribute) {
		switch (attribute) {
		case VIEW:
			return "VIEW";
		case ADD:
			return "ADD";
		case UPDATE:
			return "UPDATE";
		case DELETE:
			return "DELETE";
		default:
			throw new IllegalArgumentException("Unknown" + attribute);
		}
	}

	@Override
	public AccessLevel convertToEntityAttribute(String dbData) {
		switch (dbData) {
		case "VIEW":
			return AccessLevel.VIEW;
		case "ADD":
			return AccessLevel.ADD;
		case "UPDATE":
			return AccessLevel.UPDATE;
		case "DELETE":
			return AccessLevel.DELETE;
		default:
			throw new IllegalArgumentException("Unknown" + dbData);
		}
	}

}
