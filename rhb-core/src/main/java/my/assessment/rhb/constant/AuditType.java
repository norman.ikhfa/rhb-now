package my.assessment.rhb.constant;


/**
 * The Enum AuditType - type/category of audit trail.
 */
public enum AuditType {
	
	SECURITY("SECURITY"),
	EMAIL("EMAIL"),
	PERSON("PERSON"),
	USERACCOUNT("USERACCOUNT");

	private String value;
	
	/**
	 * Instantiates a new audit type.
	 *
	 * @param value the value
	 */
	private AuditType(String value) {
		this.value = value;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
}
