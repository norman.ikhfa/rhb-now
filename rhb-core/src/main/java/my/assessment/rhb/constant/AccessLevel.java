package my.assessment.rhb.constant;

public enum AccessLevel {
	VIEW("VIEW"), ADD("ADD"), UPDATE("UPDATE"), DELETE("DELETE");
	
	private String value;
	
	private AccessLevel(String value) {
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}
	
}
