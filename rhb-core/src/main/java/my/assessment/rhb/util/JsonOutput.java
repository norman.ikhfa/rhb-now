package my.assessment.rhb.util;

import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonView;

import lombok.Data;

@Data
public class JsonOutput<T> {

	@JsonView(View.class)
	private boolean status;

	@JsonView(View.class)
	private List<T> data = Collections.emptyList();

	@JsonView(View.class)
	private String message;

	@JsonView(View.class)
	private long recordsTotal = 0L;

	@JsonView(View.class)
	private long recordsFiltered = 0L;
	
	@JsonView(View.class)
	private T record = null;
	
	@JsonView(View.class)
	private String exportData;
	
	@JsonView(View.class)
	private String reportName;

	public interface View {
	}
}
