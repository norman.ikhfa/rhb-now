package my.assessment.rhb.util;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.font.TextAttribute;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.text.AttributedString;
import java.util.Date;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.xml.bind.DatatypeConverter;

import org.apache.commons.net.util.Base64;
import org.audit4j.core.AuditManager;
import org.audit4j.core.dto.AuditEvent;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class RHBUtil implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private final static ObjectMapper mapper;
	
	static {
		mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
	}

	public String serialize(Object object) {
		try {
			return mapper.writeValueAsString(object);
		} catch (JsonProcessingException e) {

		}
		return null;
	}
	
	public Object serialize(String object, Class<?> clazz) {
		try {
			return mapper.readValue(object, clazz);
		} catch (IOException ioe) {

		}
		return null;
	}
	
	public JsonOutput<?> toJsonOutputSerialize(String object) {
		try {
			return mapper
				      .readerWithView(JsonOutput.View.class)
				      .forType(JsonOutput.class)
				      .readValue(object);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public Date now() {
        return new Date();
    }
	
	public String getRandomNumber(int length) {
    	SecureRandom secureRandom = new SecureRandom();
        long ranno =  (secureRandom.nextLong()); 
        if (ranno < 0) {
            ranno = -1 *(ranno);
        }
            
        return (""+ranno).substring(0, length); 
    }

	public void addEvent(AuditEvent event) {
    	AuditManager.getInstance().audit(event);
    }
	
	public String drawWatermark(String rawImg, String watermark) {
		
		BufferedImage bufferedImage = null;
		String imageString = "";
        byte[] imageByte;
        try {
        	if (rawImg.startsWith("data:")) {
        		rawImg = rawImg.replaceFirst("^data:image/[^;]*;base64,?","");
        	}
        	Base64 decoder = new Base64(true);
            imageByte = decoder.decode(rawImg);
            
            ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
//            bufferedImage = ImageIO.read(bis);
            
            Image img = ImageIO.read(bis);
            img.flush();

            ImageIcon icon = new ImageIcon(imageByte);
            int gap = 15;
            bufferedImage = new BufferedImage(icon.getIconWidth(),icon.getIconHeight(), BufferedImage.TYPE_INT_RGB);

            
            // draw watermark
            Graphics2D g2d = (Graphics2D) bufferedImage.getGraphics();
            g2d.drawImage(icon.getImage(), 0, 0, null);
            g2d.setColor(Color.BLUE);
            g2d.setFont(new Font("Arial", Font.BOLD, 12));
            
            g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            
            int y = gap;
            for (String line : watermark.split("\n")) {
            	
            	AttributedString as1 = new AttributedString(line);
                as1.addAttribute(TextAttribute.BACKGROUND, Color.LIGHT_GRAY);
                g2d.drawString(as1.getIterator(), 5, y += g2d.getFontMetrics().getHeight());
            }
            
            g2d.dispose();
            
            // write back img to base64
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ImageIO.write(bufferedImage, "jpg", bos);
            
            byte[] imageBytes = bos.toByteArray();
 
            imageString = "data:image/jpeg;base64," + URLEncoder.encode(Base64.encodeBase64String(imageBytes), "UTF-8");
            
            bis.close();
            bos.close();
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        
       
		
		return imageString;
	}
	
	public String generateZip(String filename1, String filename2, String filename3, String dn)
    {
     // These are the files to include in the ZIP file
        String[] filenames = new String[]{filename1, filename2, filename3};

        // Create a buffer for reading the files
        byte[] buf = new byte[1024];
        String outFilename = "";
        try {
            // Create the ZIP file
            outFilename = dn+".zip";
            ZipOutputStream out = new ZipOutputStream(new FileOutputStream(outFilename));

            // Compress the files
            for (int i=0; i<filenames.length; i++) {
                FileInputStream in = new FileInputStream(filenames[i]);

                // Add ZIP entry to output stream.
                out.putNextEntry(new ZipEntry(filenames[i]));

                // Transfer bytes from the file to the ZIP file
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }

                // Complete the entry
                out.closeEntry();
                in.close();
            }

            // Complete the ZIP file
            out.close();
        } catch (IOException e) 
        { 
            e.printStackTrace();
        }
        
        return outFilename;
    }
	
	public static String getFileChecksum(File file) {
		try {
			byte[] b = Files.readAllBytes(file.toPath());
       	 	byte[] hash = MessageDigest.getInstance("MD5").digest(b);
       	 
   	 		String checksum = DatatypeConverter.printHexBinary(hash);
   	 		return checksum;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}

}
