package my.assessment.rhb.util;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import my.assessment.rhb.constant.AccessLevel;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface RHBModule {

	AccessLevel[] accessLevel() default AccessLevel.VIEW;
	
	String moduleName() default "";
	
	String rootUrl() default "/";
	
	boolean coreType() default false;
}
